import requests
import simplejson as json
import json
from flask import Flask, request, Response
from flask import send_from_directory
from flask import send_file
import io
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import re
import boto3
from io import StringIO


# config
app = Flask(__name__)
app.config["PROPAGATE_EXCEPTIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'full'
api = Api(app, version='1.0', title='Big7_Api')

# parser for Big7-login
parser = api.parser()
parser.add_argument('token', type=str, required=False,
                    help='Bearer Access Token.')

parser.add_argument(
    "username", type=str, required=True, help="username")

parser.add_argument(
    "password", type=str, required=True, help="password")


# parser for Big7-Check
parser_big7_check = api.parser()
parser_big7_check.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_big7_check.add_argument(
    "key", type=str, required=True, help="key")


# parser for Big7-send-message
parser_big7_send_message = api.parser()

parser_big7_send_message.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_big7_send_message.add_argument(
    "JSON", type=str, required=False, help="JSON string")                               



# parser for get-megssage-big7
parser_big7_get_message = api.parser()
parser_big7_get_message.add_argument('token', type=str, required=False,
                               help='Bearer Access Token.')

parser_big7_get_message.add_argument(
    "key", type=str, required=True, help="key")

parser_big7_get_message.add_argument(
    "url", type=str, required=True, help="url")

# define for auth
parser_credentials = api.parser()

parser_credentials.add_argument(
    "username", type=str, required=True, help="username")

parser_credentials.add_argument(
    "password", type=str, required=True, help="password")

CORS(app, origins=r'*', allow_headers=r'*')

# models
token = fields.String(example='"eyJraWQiOiI5M2NHeEY5dHhINTl2SWJpVDR1NDZQQ0VvOFNGZ2xEdno5THg1UVRcL2k3Yz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjOWRmNTUzMC0zOGQ3LTQzMTEtODM5Zi00OThmNTMzZDU0ZmYiLCJjb2duaXRvOmdyb3VwcyI6WyJiYXNpY3BsYW4iXSwiZXZlbnRfaWQiOiI5MzA2Yjk3NC1hZWE3LTQ4MGYtYjY4Yi1kZDhjZTkzMzhmZmMiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjAyMDY1NDEzLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9uZTYwczlEUXkiLCJleHAiOjE2MDIwNjkwMTMsImlhdCI6MTYwMjA2NTQxMywianRpIjoiN2QxYjI4YmYtYmIxNS00MzcyLWFiYjktN2ExZmRmMDUzOWM2IiwiY2xpZW50X2lkIjoiM3JzanRidHNmMmJ2NWYxODdkOTk4OWQyc2wiLCJ1c2VybmFtZSI6ImM5ZGY1NTMwLTM4ZDctNDMxMS04MzlmLTQ5OGY1MzNkNTRmZiJ9.RS-tvI0-Fdb8gvJAxvr2jVSs3ChhPAlXEGGkJu2EoA28PgVdKFfqui9IRzHtGVZi4CDpRs8ZAsA7g7FApF5bJmONnWeu2WWpghMsuEZFixiaEfVlCajhukMZZZC_KiAyxCdKGwti5_DbvWpmGBwdqdlQOWOLhZFPCm2XgnxFp_3ANrqZEJhXwGDTekxl-AMyb1L1s-msE6-5EgVqv9ZggTg9eEwNFc_VfKw87ahDP8sK9Qgz6ZPTsozhCo9vXMQ-YfAlAAicIiK-nJoSwo3fhjLMF-rgcFPySDS9ePWKb08vJfyWLp3-itZHwmn6d5ULKJkE0Q8e-5SSUNfUIX1dxQ"')

# login für big7


@api.route('/big7-login', doc={"description": "Login at Big7 with Big7 credentials"})
class LoginBig7(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser)
    def post(self):
        args = parser.parse_args()

        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # Log into the chat
        isLogged = login_big7(args["username"], args["password"])

        return isLogged

# login Big7 parameter l for login-id and p for password


def login_big7(profile, password):

    url = "https://www.big7.com/login.html"

    payload = {'do': '1',
               'cookies': 'true',
               'nickname': profile,
               'passwort': password}
    files = [

    ]
    headers = {
        'authority': 'www.big7.com',
        'method': 'POST',
        'path': '/login.html',
        'scheme': 'https',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'cache-control': 'max-age=0',
        'content-type': 'application/x-www-form-urlencoded',
        'origin': 'https://www.big7.com',
        'referer': 'https://www.big7.com/',
        'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36'
    }

    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files)
    my_cookies = requests.utils.dict_from_cookiejar(response.cookies)
    key = my_cookies['B7SID']

    if response.text[:300].find(key) != -1:
        return get_in_big7(key, profile)
    else:
        return False


# login part 2, parameter is cookie from login
def get_in_big7(key, profile):

    url = "https://www.big7.com/meinbigseven.html"

    payload = {'ajax': 'Y',
               'ajaxy_height': '827',
               'ajaxy_width': '1440',
               'controller': 'ajaxy-page',
               'B7SID': key}

    files = [
    ]

    headers = {
        'authority': 'www.big7.com',
        'method': 'POST',
        'path': '/meinbigseven.html',
        'scheme': 'https',
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
        'content-length': '101',
        'content-type': 'application/x-www-form-urlencoded',
        'origin': 'https://www.big7.com',
        'referer': 'https://www.big7.com/meinbigseven.html',
        'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
    }

    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files)

    if len(response.text) > 10:
        succ = {
            "profile": profile,
            "key": key
        }
        return succ
    else:
        return False


# check für big7
@api.route('/big7-check', doc={"description": "check for new messages"})
class CheckBig7(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_big7_check)


    def post(self):
        args = parser_big7_check.parse_args()

        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = check_msg_big7(args["key"])

        return rslt

# check for new messages, parameter c is cookie, if no message end here, else get_html
def check_msg_big7(key):

    url = "https://www.big7.com/chatv3.php"

    payload={'ajax': 'C',
    'do': 'get_chat_status_msg',
    'B7SID':key}
    files=[

    ]
    headers = {
      'authority': 'www.big7.com',
      'method': 'POST',
      'path': '/chatv3.php',
      'scheme': 'https',
      'accept': '*/*',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
      'content-length': '76',
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'origin': 'https://www.big7.com',
      'referer': 'https://www.big7.com/meinbigseven.html',
      'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
      'sec-ch-ua-mobile': '?0',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
      'x-requested-with': 'XMLHttpRequest'
    }
    
    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    jsn = json.loads(response.text)
    
    if jsn['LOGIN'] == "N":
        return False
    elif jsn['CURRENT_NEW_MESSAGES'] != 0:
        return get_html_big7(key)
    else:
        return('No Unread')


# get whole html of the page and send to find_url as text, key for cookie
def get_html_big7(key):
    url = "https://www.big7.com/messages.html"
    
    payload={
        'B7SID':key ,
    }
    
    headers = {
    }
    
    response = requests.request("POST", url, headers=headers, data=payload)
    return find_url_big7(response.text, key)


# find url from text and then get message
def find_url_big7(text, key):

    from_string = text.find('nmsg_new')
    to_string = from_string + 360
    
    search_string_href = text[from_string:to_string]
    
    found_string_href = search_string_href.find('<a href="') + 9
    
    search_string_url = search_string_href[found_string_href:]
    
    message_url = search_string_url.split('"')[0]
    
    if message_url == '':
        return 'No Unread'
    else:
        return get_message_big7_html(key, message_url)

# get message url, c for cookie, url for message url
def get_message_big7_html(key, url):
    url = url

    payload={'ajax': 'Y',
    'ajaxy_height': '827',
    'ajaxy_width': '1440',
    'controller': 'ajaxy-msg',
    'B7SID': key
    }
    files=[

    ]
    headers = {
      'authority': 'www.big7.com',
      'method': 'POST',
      'path': 'https://www.big7.com/messages',
      'scheme': 'https',
      'accept': '*/*',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
      'content-length': '76',
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'cookie': 'B7SID={};'.format(key),
      'origin': 'https://www.big7.com',
      'referer': 'https://www.big7.com/messages/.html',
      'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
      'sec-ch-ua-mobile': '?0',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
      'x-requested-with': 'XMLHttpRequest'
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    from_str = response.text.find('<!-- EOF show_rand -->')
    to_str = response.text.find('<div class="nmsg_filler"></div>')
    new_message = response.text[from_str:to_str]
    return prepare_big7_html(response.text, new_message, url)

# delete false symbols in html to be displayed in vue
def prepare_big7_html(fullhtml, html, url):

    final_message = html.replace('0"" ', '" ')
    final_message = final_message.replace('1"" ', '" ')
    final_message = final_message.replace('2"" ', '" ')
    final_message = final_message.replace('3"" ', '" ')
    final_message = final_message.replace('4"" ', '" ')
    final_message = final_message.replace('5"" ', '" ')
    final_message = final_message.replace('6"" ', '" ')
    final_message = final_message.replace('7"" ', '" ')
    final_message = final_message.replace('8"" ', '" ')
    final_message = final_message.replace('9"" ', '" ')
    
    matches = re.finditer(r"data-nmsgform=\"[0-9, a-z]", fullhtml, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):
        nmsg_num = fullhtml[(match.end()-1):]

    nmsg_num = nmsg_num.split('"')
    nmsg_num = nmsg_num[0]




    succ = {
        "html":final_message,
        "url":url,
        "msgKey": nmsg_num
    }

    return succ




# senden für big7
@api.route('/big7-send-message', doc={"description": "send message"})
class getMessageBig7(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_big7_send_message)
    def post(self):
        args = parser_big7_send_message.parse_args()
        payload = request.get_json()


        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = big7_send_message(payload)


#send message
def big7_send_message(jsn):
    url = jsn['url']

    payload={
    'do': '1',
    'ajax': 'Y',
    'roomName': jsn['conversationId'],
    'e_id': jsn['customerId'],
    'kosten': '0',
    'B7SID': jsn['key'],
    'forward_to': '0',
    'nmsg_form': jsn['msgKey'],
    'chatbridge': 'websocket',
    'nachricht': jsn['msgContent']
    }
    files=[
    ]

    headers = {
        'Accept':'Application/json',
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    return response.text




# check für big7 - wird momentan nicht verwendet
@api.route('/big7-get-message', doc={"description": "check for new messages"})
class getMessageBig7(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_big7_get_message)
    def post(self):
        args = parser_big7_get_message.parse_args()

        # don't check auth on local host
        #req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        #if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # get cookies
        rslt = get_message_big7(args["key"], args["url"])

        return rslt

# get message url, c for cookie, url for message url
def get_message_big7(key, url):
    url = url

    payload={'ajax': 'Y',
    'ajaxy_height': '827',
    'ajaxy_width': '1440',
    'controller': 'ajaxy-msg',
    'B7SID': key
    }
    files=[

    ]
    headers = {
      'authority': 'www.big7.com',
      'method': 'POST',
      'path': 'https://www.big7.com/messages',
      'scheme': 'https',
      'accept': '*/*',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'de,en-US;q=0.9,en;q=0.8,th;q=0.7',
      'content-length': '76',
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'cookie': 'B7FVISIT={};'.format(key),
      'origin': 'https://www.big7.com',
      'referer': 'https://www.big7.com/messages/.html',
      'sec-ch-ua': '"Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"',
      'sec-ch-ua-mobile': '?0',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'same-origin',
      'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
      'x-requested-with': 'XMLHttpRequest'
    }

    response = requests.request("POST", url, headers=headers, data=payload, files=files)
    from_str = response.text.find('<!-- EOF show_rand -->')
    to_str = response.text.find('<div class="nmsg_filler"></div>')
    new_message = response.text[from_str:to_str]
    return prepare_big7(new_message)

# delete false symbols in html to be displayed in vue
def prepare_big7(html):

    final_message = html.replace('0"" ', '" ')
    final_message = final_message.replace('1"" ', '" ')
    final_message = final_message.replace('2"" ', '" ')
    final_message = final_message.replace('3"" ', '" ')
    final_message = final_message.replace('4"" ', '" ')
    final_message = final_message.replace('5"" ', '" ')
    final_message = final_message.replace('6"" ', '" ')
    final_message = final_message.replace('7"" ', '" ')
    final_message = final_message.replace('8"" ', '" ')
    final_message = final_message.replace('9"" ', '" ')
    
    return final_message




# auth
@api.route("/oauth/token", doc={
    "description": "Returns a Bearer authentication token. The token is valid for 30 minutes."
})
class awsAuth(Resource):
    @api.expect(parser_credentials)
    @api.response(200, 'Success', token)
    @api.response(401, 'Unauthorized Error')
    def get(self):
        args = parser_credentials.parse_args()
        username = args["username"]
        password = args["password"]
        try:
            client = boto3.client("cognito-idp", "eu-central-1")
            response = client.initiate_auth(
                AuthFlow="USER_PASSWORD_AUTH",
                AuthParameters={"USERNAME": username, "PASSWORD": password, },
                ClientId="41ft5d2kpm37ctefvbf0pb3m29",
                ClientMetadata={"UserPoolId": "eu-central-1_o8ZrmNcQP"},
            )
        except:
            raise Unauthorized('That email/password combination is not valid.')

        Token = response["AuthenticationResult"]["AccessToken"]
        return Token


# get user
def get_user_name(token):
    region = "eu-central-1"
    provider_client = boto3.client("cognito-idp", region_name=region)
    try:
        user = provider_client.get_user(AccessToken=token)
    except provider_client.exceptions.NotAuthorizedException as e:
        raise Unauthorized('Invalid token!')
    return user["Username"]


if __name__ == "__main__":
    app.run()
